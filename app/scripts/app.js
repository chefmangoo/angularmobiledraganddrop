'use strict';

angular
    .module('dragdropApp', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute',
        'ngDragDrop'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/dragdrop', {
                templateUrl: 'views/dragdrop.html',
                controller: 'DragdropCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
