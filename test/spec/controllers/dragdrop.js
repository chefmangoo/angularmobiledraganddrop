'use strict';

describe('Controller: DragdropCtrl', function () {

  // load the controller's module
  beforeEach(module('dragdropApp'));

  var DragdropCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DragdropCtrl = $controller('DragdropCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
